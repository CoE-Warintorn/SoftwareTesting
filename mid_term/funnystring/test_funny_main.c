#include <stdarg.h>
#include <memory.h>
#include <setjmp.h>
#include <cmocka.h>
#include <string.h>

#include "funny_string.h"

static void test_funny (char *input, char *expected_output, int expected_distance) {
    int distance;
    char output[10];

    distance = funny_string(input, output);

    assert_int_equal(expected_distance, distance);
    assert_string_equal(expected_output, output);
}

static void test_give_acxz_should_be_funny (void **state)
{
    (void) state;
    test_funny("acxz", "funny", 3);
}

static void test_give_afghm_should_be_funny (void **state)
{
    (void) state;
    test_funny("afghm", "funny", 4);
}

static void test_give_abxz_should_be_not_funny (void **state)
{
    (void) state;
    test_funny("abxz", "not funny", 0);
}

static void test_give_any_should_be_funny (void **state)
{
    (void) state;
    int i, n = 26*100;
    char input[n+1], c = 'z';
    for (i=0; i<n; i++)
    {
        input[i] = c;
        c = (c <= 'a') ? 'z' : c - 1;
    }
    test_funny(input, "funny", n-1);
}

int main ()
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_give_acxz_should_be_funny),
        cmocka_unit_test(test_give_afghm_should_be_funny),
        cmocka_unit_test(test_give_abxz_should_be_not_funny),
        cmocka_unit_test(test_give_any_should_be_funny)
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}