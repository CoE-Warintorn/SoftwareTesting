#include <string.h>

#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "fizz_buzz.h"

static void input_generator(int input[], int input_size, int number)
{
    int i = 0 , index = 0;
    while (index < input_size) {
        if (number == 3) {
            if (3*i%5 != 0) {
                input[index] = 3*i;
                index++;
            }
        }
        else if (number == 5) {
            if (5*i%3 != 0) {
                input[index] = 5*i;
                index++;
            }
        }
        else if (number == 15) {
            input[index] = 15*i;
            index++;
        }
        else {
            if (i%3 != 0 && i%5 != 0) {
                input[index] = i;
                index++;
            }
        }
        i++;
    }
}

static void input_fizz_buzz_test(int input_size, int test_input, char *expected_output)
{
    int input[input_size], i;
    char output[10];

    input_generator(input, input_size, test_input);

    for (i=0; i<input_size; i++) {
        fizz_buzz(input[i], output);
        assert_string_equal(expected_output, output);
    }
}

static void test_given_3_100times_should_be_fizz(void **state) {
    (void) state;
    input_fizz_buzz_test(100, 3, "Fizz");
}

static void test_given_5_100times_should_be_buzz(void **state) {
    (void) state;
    input_fizz_buzz_test(100, 5, "Buzz");
}

static void test_given_15_100times_should_be_fizzbuzz(void **state) {
    (void) state;
    input_fizz_buzz_test(100, 15, "FizzBuzz");
}

static void test_given_any_100times_should_not_be_fizz_and_buzz_and_fizzbuzz(void **state) {
    (void) state;
    input_fizz_buzz_test(100, 1, "");
}

int main () {
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_given_3_100times_should_be_fizz),
        cmocka_unit_test(test_given_5_100times_should_be_buzz),
        cmocka_unit_test(test_given_15_100times_should_be_fizzbuzz),
        cmocka_unit_test(test_given_any_100times_should_not_be_fizz_and_buzz_and_fizzbuzz)
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}