#include <string.h>
#include "fizz_buzz.h"

void fizz_buzz(int x, char *output)
{
    if ((x%3 || x%5) == 0)
        strcpy(output, "FizzBuzz");
    else if (x%3 == 0)
        strcpy(output, "Fizz");
    else if (x%5 == 0)
        strcpy(output, "Buzz");
    else
        strcpy(output, "");
}