#include <stdio.h>
#include "binary_search.h"


int main(){
    int key = 5;
    int input[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    int output = binary_search(input, 0, 9, key);

    printf("Index of %d is %d\n", key, output);
    return 0;
}
