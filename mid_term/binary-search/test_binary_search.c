#include <stdarg.h>
#include <memory.h>
#include <setjmp.h>
#include <cmocka.h>

#include "binary_search.h"

static void input_generator(int *input, int size)
{
    int i, min = -(size/2);
    for (i=0; i<size; i++)
        input[i] = min++;
}

static void test_search_should_be_found(void **state)
{
    (void) state;
    int i, j, size = 100;
    int input[size];
    int output;
    input_generator(input, size);
    
    for (i=0; i<size-1; i++) {
        output = binary_search(input, i, size-1, input[size-1]);
        assert_int_equal(size-1, output);
    }
        
}

static void test_search_should_be_not_found_in_range()
{
    int i, j, size = 100;
    int input[size];
    int output;
    input_generator(input, size);
    
    for (i=0; i<size-1; i++) {
        output = binary_search(input, i, size-1, size);
        assert_int_equal(-(size+1), output);
    }
        
}

static void test_search_should_be_not_found_out_range_left()
{
    int i, j, size = 100;
    int input[size];
    int output;
    input_generator(input, size);
    
    for (i=0; i<size-1; i++) {
        output = binary_search(input, i, size-1, input[i]-1);
        assert_int_equal(-(i+1), output);
    }
        
}

static void test_search_should_be_not_found_out_range_right()
{
    int i, j, size = 100;
    int input[size];
    int output;
    input_generator(input, size);
    
    for (i=0; i<size-1; i++) {
        output = binary_search(input, i, size-1, input[size-1]+1);
        assert_int_equal(-(size+1), output);
    }
        
}

int main ()
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_search_should_be_found),
        cmocka_unit_test(test_search_should_be_not_found_in_range),
        cmocka_unit_test(test_search_should_be_not_found_out_range_left),
        cmocka_unit_test(test_search_should_be_not_found_out_range_right),
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}