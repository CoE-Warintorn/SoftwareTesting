install cmocka
    sudo apt install libcmocka-dev

compile with gcc
    gcc file_name.c -o file_name -lcmocka

install gcovr
    sudo apt install gcovr

compile gcovr
    gcc file_name.c -o file_name -lcmocka -fprofile-arcs -ftest-coverage -fPIC -O0

run gcovr
    gcovr .

    gcovr . --html --html-detail -o filename.html

install Make
    sudo apt install make

using
    make